﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootballFun.Models
{
    public class Team
    {
        public string Name;
        public string League;
        public int Id;

        // add a constructor
        public Team(string name, string league, int id)
        {
            this.Name = name;
            this.League = league;
            this.Id = id;
        }
    }
}