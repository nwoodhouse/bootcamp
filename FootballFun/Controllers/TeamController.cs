﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using FootballFun.Models;

namespace FootballFun.Controllers
{
    public class TeamController : ApiController
    {
        private static Dictionary<int, Team> Teams = new Dictionary<int, Team>()
        {
            { 12, new Team("Man U", "Premier", 12) },
            { 2, new Team("Liverpool", "Premier", 2) },
            { 7, new Team("Cinderford FC", "S League South, Div 1, SW", 7) }
        };

        // GET: api/Team
        public IEnumerable<Team> Get()
        {
            return TeamController.Teams.Values;
        }

        // GET: api/Team/5
        public Team Get(int id)
        {
            return TeamController.Teams[id];
        }

        // POST: api/Team
        // should create a given resource!
        public void Post()
        {
            // take the current user id from the session!
            object teamObj = HttpContext.Current.Session["t"];
            if (teamObj != null)
            {
                Team team = (Team)teamObj;
            }
        }

        // PUT: api/Team/5
        // ammend the record identified by the given id
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Team/5
        // should remove the resource identified by the given id
        public void Delete(int id)
        {
            TeamController.Teams.Remove(id);
        }
    }
}
