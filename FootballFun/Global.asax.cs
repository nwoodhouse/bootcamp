﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

// add the using statements as directed in the handy readme.txt!
using System.Web.Http;

namespace FootballFun
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // tell the ASP.NET runtime to configure the Web API routing rules!
            GlobalConfiguration.Configure(WebApiConfig.Register);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
