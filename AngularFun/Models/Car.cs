﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularFun.Models
{
    public class Car
    {
        public string Img;
        public string Make;
        public int Price;

        public Car(string make, string img, int price) {
            this.Make = make;
            this.Img = img;
            this.Price = price;
        }
    }
}